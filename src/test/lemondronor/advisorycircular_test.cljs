(ns lemondronor.advisorycircular-test
  (:require [cljs.test :refer (async deftest is testing)]
            [kitchen-async.promise :as p]
            [lemondronor.advisorycircular :as advisorycircular]
            [lemondronor.advisorycircular.pelias :as pelias]
            [lemondronor.advisorycircular.util :as util]))


(def epsilon 0.0000010)

(defn a= [a b]
  (< (Math/abs (- a b)) epsilon))

(defn strmatch [pattern text]
  (if (string? text)
    (re-find pattern text)
    false))


;; (deftest bearing->angle
;;   (is (angles= (cooleradar/bearing->angle 0) (/ Math/PI 2)))
;;   (is (angles= (cooleradar/bearing->angle (/ Math/PI 2)) 0))
;;   (is (angles= (cooleradar/bearing->angle Math/PI) (* 2 (/ 3 4) Math/PI))))

(deftest parse-adsbexchange-ac-element
  (let [ac {"gnd" "0", "trt" "2", "pos" "1", "call" "", "mil" "0", "ttrk" "",
            "dst" "14.6", "reg" "HL7634", "altt" "0", "cou" "South Korea",
            "postime" "1575488288571", "galt" "139", "mlat" "0", "spd" "10.5",
            "sqk" "", "talt" "", "wtc" "3", "alt" "100", "lon" "-118.416438",
            "opicao" "AAR", "interested" "0", "trak" "264.4",
            "trkh" "0", "icao" "71BE34", "lat" "33.937908", "vsit" "1",
            "tisb" "0", "vsi" "0", "sat" "0"}
        r (advisorycircular/parse-adsbexchange-ac-element ac)]
    (is (= (:icao r) "71BE34"))
    (is (= (:registration r) "HL7634"))
    (is (= (:callsign r) nil))
    (is (a= (:lon r) -118.416438))
    (is (a= (:lat r) 33.937908))
    (is (a= (:alt r) 100))
    (is (a= (:speed r) 10.5))
    (is (= (:squawk r) nil))
    (is (not (:military? r)))
    (is (not (:mlat? r)))
    (is (a= (:postime r) 1575488288571))))


(deftest prune-history
  (let [hist [{:time 0 :id 0} {:time 1000000 :id 1} {:time 2000000 :id 2}]]
    (is (= (advisorycircular/prune-history hist 2500000 advisorycircular/default-config)
           [{:time 2000000 :id 2}]))))


(deftest update-history-db-record
  (testing "Updating an existing record"
    (let [db  {"0" {:icao "0"
                    :lat 0
                    :lon 0
                    :postime 3000
                    :history [{:time 1000 :id 0}
                              {:time 2000 :id 1}
                              {:time 3000  :id 2}]}
               "1" :anything}
          record {:icao "0"
                  :lat 1
                  :lon 1
                  :postime 3500}]
      (is (= (advisorycircular/update-history-db-record db record)
             {"0" {:icao "0"
                   :lat 1
                   :lon 1
                   :postime 3500
                   :history [{:time 1000 :id 0}
                             {:time 2000 :id 1}
                             {:time 3000 :id 2}
                             {:time 3500 :lat 1 :lon 1 :alt nil :gnd? nil}]}
              "1" :anything}))))
  (testing "Adding a new record"
    (let [db {"0" {:icao "0"
                   :lat 0
                   :lon 0
                   :postime 3000
                   :history [{:time 1000 :id 0}
                             {:time 2000 :id 1}
                             {:time 3000 :id 2}]}
              "1" :anything}
          record {:icao "2"
                  :lat 1
                  :lon 1
                  :postime 3500}]
      (is (= (advisorycircular/update-history-db-record db record)
             {"0" {:icao "0"
                   :lat 0
                   :lon 0
                   :postime 3000
                   :history [{:time 1000 :id 0}
                             {:time 2000 :id 1}
                             {:time 3000 :id 2}]}
              "1" :anything
              "2" {:icao "2"
                   :lat 1
                   :lon 1
                   :postime 3500
                   :history [{:time 3500 :lat 1 :lon 1 :alt nil :gnd? nil}]}})))))


(deftest expand-template
  (let [data {:locality "Palmdale"
              :continent "North America"
              :military? true
              :alt 3850
              :speed "209"
              :normalized-curviness 14.768651250300287
              :accuracy "centroid"
              :country_a "USA"
              :continent_gid "whosonfirst:continent:102191575"
              :name "Palmdale"
              :squawk "5330"
              :icao "AE1482"
              :county_a "LO"
              :county "Los Angeles County"
              :source "whosonfirst"
              :gid "whosonfirst:locality:85923493"
              :curviness 1269.8089810739468
              :locality_gid "whosonfirst:locality:85923493"
              :region "California"
              :militaryicao "AE1482"
              :region_a "CA"
              :nearbydistance 8.167
              :callsign "RAIDR49"
              :layer "locality"
              :mlat? false
              :country_gid "whosonfirst:country:85633793"
              :label "Palmdale, CA, USA"
              :id "85923493"
              :lon -118.00375
              :region_gid "whosonfirst:region:85688637"
              :lat 34.661074
              :militaryregistration "166765"
              :county_gid "whosonfirst:county:102086957"
              :started-circling-time 1576266715691
              :distance 6.855
              :source_id "85923493"
              :registration "166765"
              :confidence 0.5
              :country "United States"
              :postime 1576266689756
              :nearbylandmark "Living Faith Foursquare Church"}]
    (is (strmatch #"military" (-> (advisorycircular/expand-template data) :text))))
  (let [data {:locality "Palmdale"
              :continent "North America"
              :military? true
              :alt 3200
              :speed "161"
              :normalized-curviness 15.783422690487765
              :accuracy "centroid"
              :country_a "USA"
              :continent_gid "whosonfirst:continent:102191575"
              :name "Palmdale"
              :squawk "5330"
              :icao "AE1482"
              :county_a "LO"
              :county "Los Angeles County"
              :source "whosonfirst"
              :gid "whosonfirst:locality:85923493"
              :curviness 1098.803548060181
              :locality_gid "whosonfirst:locality:85923493"
              :region "California"
              :militaryicao "AE1482"
              :region_a "CA"
              :nearbydistance 7.828
              :callsign "RAIDR49"
              :layer "locality"
              :mlat? false
              :country_gid "whosonfirst:country:85633793"
              :label "Palmdale, CA, USA"
              :id "85923493"
              :lon -118.049183
              :region_gid "whosonfirst:region:85688637"
              :lat 34.649808
              :militaryregistration "166765"
              :county_gid "whosonfirst:county:102086957"
              :started-circling-time 1576267564959
              :distance 6.336
              :source_id "85923493"
              :registration "166765"
              :confidence 0.5
              :country "United States"
              :postime 1576267555709
              :nearbylandmark "Living Faith Foursquare Church"}]
    (is (strmatch #"military" (-> (advisorycircular/expand-template data) :text))))
  (testing "a vs. an for type"
    (let [data {:registration "TEST" :icao "123" :type "Airbus 380" :locality "Test City"}]
      (is (strmatch #"an Airbus" (:text (advisorycircular/expand-template data)))))
    (let [data {:registration "TEST" :icao "123" :type "Yoyo 380" :locality "Test City"}]
      (is (strmatch #"a Yoyo" (:text (advisorycircular/expand-template data)))))))


(deftest merge-adsbx-db-rec
  (is (= (advisorycircular/merge-adsbx-aircraft-db-rec {:registration "N1"}
                                                       {:registration "N2" :type "B52"})
         {:registration "N1", :type "B52"}))
  (is (= (advisorycircular/merge-adsbx-aircraft-db-rec {:registration nil}
                                                       {:registration "N2" :type "B52"})
         {:registration "N2", :type "B52"}))
  (is (= (advisorycircular/merge-adsbx-aircraft-db-rec {:registration "N1"}
                                                       {:registration "N2" :type nil})
         {:registration "N1", :type nil})))


(deftest generate-description
  (testing "Basic generation"
    (let [ac {:icao "B00B00"
              :registration "NBADB0Y"}
          sqb {:registration "NGOODB0Y"
               :type "B52"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby nil
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"NBADB0Y" desc))
      (is (strmatch #"a B52" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"#NBADB0Y" desc))))
  (testing "Missing ADSBX registration"
    (let [ac {:icao "B00B00"}
          sqb {:registration "NGOODB0Y"
               :type "B52"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby {:name "Disneyland" :distance 2}
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"NGOODB0Y" desc))
      (is (strmatch #"a B52" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"#NGOODB0Y" desc))))
  (testing "foo"
    (let [ac {:military? false :alt 1300 :speed 72.1 :squawk "1200"
              :icao "AAE0C2" :type nil, :callsign "N80NT", :registration nil}
          sqb {:registration "N80NT", :type "Eurocopter Squirrel AS 350 B2"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby {:name "Disneyland" :distance 2}
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"N80NT" desc))
      (is (strmatch #"a Eurocopter Squirrel AS 350 B2" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"1300 feet" desc))
      (is (strmatch #"speed 83 MPH" desc))
      (is (strmatch #"squawking 1200" desc))
      (is (strmatch #"#N80NT" desc)))))


(deftest filter-landmarks
  (testing "filter-landmarks"
    (let [landmarks [{:properties {:name "Johnny Depp"}}
                     {:properties {:name "Musso & Frank's"}}
                     {:properties {:name "Johnny Depp's Star"}}]]
      (is (= (advisorycircular/filter-landmarks {:blocklist ["Johnny Depp"]}
                                                landmarks)
             [{:properties {:name "Musso & Frank's"}}]))
      (is (= (advisorycircular/filter-landmarks {:blocklist ["Frank"]}
                                                landmarks)
             [{:properties {:name "Johnny Depp"}}
              {:properties {:name "Johnny Depp's Star"}}])))))


(def r2508-data1
  [{:accuracy "point"
    :confidence 0.5
    :continent "North America"
    :continent_gid "whosonfirst:continent:102191575"
    :country "United States"
    :country_a "USA"
    :country_gid "whosonfirst:country:85633793"
    :county "San Bernardino County"
    :county_a "SA"
    :county_gid "whosonfirst:county:102085395"
    :distance 4.261
    :gid "openstreetmap:venue:node/358808120"
    :id "node/358808120"
    :label "Haystack Butte, San Bernardino County, CA, USA"
    :layer "venue"
    :name "Haystack Butte"
    :region "California"
    :region_a "CA"
    :region_gid "whosonfirst:region:85688637"
    :source "openstreetmap"
    :source_id "node/358808120"}
   {:accuracy "centroid"
    :alt 5825
    :callsign "COBRA02"
    :confidence 0.5
    :continent "North America"
    :continent_gid "whosonfirst:continent:102191575"
    :country "United States",
    :country_a "USA"
    :country_gid "whosonfirst:country:85633793"
    :county "San Bernardino County"
    :county_a "SA"
    :county_gid "whosonfirst:county:102085395"
    :curviness 1463.4205557421808
    :distance 132.69
    :gid "whosonfirst:county:102085395"
    :icao "AE264F"
    :id "102085395"
    :label "San Bernardino County, CA, USA"
    :lat 34.884804
    :layer "county"
    :lon -117.629528
    :military? true
    :militaryicao "AE264F"
    :militaryregistration "73-1215?"
    :mlat? false
    :name "San Bernardino County"
    :nearbydistance "2.65"
    :nearbylandmark "Haystack Butte"
    :normalized-curviness 14.188938867970446
    :postime 1581098213228
    :region "California"
    :region_a "CA"
    :region_gid "whosonfirst:region:85688637"
    :registration "73-1215?"
    :source "whosonfirst"
    :source_id "102085395"
    :speed "226"
    :squawk "0026"
    :started-circling-time 1581098223891
    :type "Beechcraft C-12C Huron"}
   {:continent "North America"
    :military? true
    :alt 14000
    :speed "139"
    :normalized-curviness 6.97888492071943
    :accuracy "centroid"
    :country_a "USA"
    :continent_gid "whosonfirst:continent:102191575"
    :name "Kern County"
    :squawk "0006"
    :icao "AE2651"
    :county_a "KE"
    :county "Kern County"
    :type "Beechcraft C-12C Huron"
    :source "whosonfirst"
    :gid "whosonfirst:county:102081727"
    :curviness 1447.2660076871339
    :region "California"
    :militaryicao "AE2651"
    :region_a "CA"
    :nearbydistance "1.93"
    :callsign "COBRA37"
    :layer "county"
    :mlat? false
    :country_gid "whosonfirst:country:85633793"
    :label "Kern County, CA, USA"
    :id "102081727"
    :lon -117.654247
    :region_gid "whosonfirst:region:85688637"
    :lat 35.076273
    :militaryregistration "76-0166"
    :county_gid "whosonfirst:county:102081727"
    :started-circling-time 1588785019767
    :distance 101.122
    :source_id "102081727"
    :registration "76-0166"
    :confidence 0.5
    :country "United States"
    :postime 1588785014065
    :nearbylandmark "Russell Mine"}])


(deftest r2508-description
  (testing "R-2508 generation"
    (is (= (advisorycircular/expand-template (nth r2508-data1 0))
           nil))
    (is (= (:text (advisorycircular/expand-template (nth r2508-data1 1)))
           (str "73-1215?, a military Beechcraft C-12C Huron, (callsign COBRA02)"
                " is circling over San Bernardino County at 5825 feet, speed 226"
                " MPH, squawking 0026, 2.65 miles from Haystack Butte #73_1215?"
                " https://globe.adsbexchange.com/?icao=AE264F&zoom=13")))
    (is (= (:text (advisorycircular/expand-template (nth r2508-data1 2)))
           (str "76-0166, a military Beechcraft C-12C Huron, (callsign COBRA37)"
                " is circling over Kern County at 14000 feet, speed 139 MPH,"
                " squawking 0006, 1.93 miles from Russell Mine #76_0166"
                " https://globe.adsbexchange.com/?icao=AE2651&zoom=13"))))
  (testing "foo"
    (let [ac {:military? false :alt 1300 :speed 72.1 :squawk "1200"
              :icao "AAE0C2" :type nil, :callsign "N80NT", :registration nil}
          sqb {:registration "N80NT", :type "Eurocopter Squirrel AS 350 B2"}
          reverse {:properties {:neighbourhood "Silver Lake" :locality "Los Angeles"}}
          nearby {:name "Disneyland" :distance 2}
          desc (advisorycircular/generate-description ac sqb reverse nearby)]
      (is (strmatch #"N80NT" desc))
      (is (strmatch #"a Eurocopter Squirrel AS 350 B2" desc))
      (is (strmatch #"Silver Lake.*Los Angeles" desc))
      (is (strmatch #"1300 feet" desc))
      (is (strmatch #"speed 83 MPH" desc))
      (is (strmatch #"squawking 1200" desc))
      (is (strmatch #"#N80NT" desc)))))


(deftest closest-airport
  (let [a1 {:properties {:distance 1.5 :label "Bridge heliport"}}
        a2 {:properties {:distance 0.5 :label "New York Seaport"}}
        a3 {:properties {:distance 1.0 :label "LAX"}}
        nearby (fn [_config _lat _lon _options]
                 {:features [a1 a2 a3]})]
    ;; This is pretty hacky, but.
    (set! pelias/nearby nearby)
    (println (nearby 1 2 3 4))
    (async
     done
     (p/do
       (testing "closest-airport 1"
         (p/let [r (advisorycircular/closest-airport {} 0 0)]
           (is (= r a2))))
       (testing "closest-airport with blocklist"
         (let [conf {:airport {:blocklist ["seaport"]}}]
           (p/let [r (advisorycircular/closest-airport conf 0 0)]
             (is (= r a3)))))
       (done)))))


(deftest validate-config
  (testing "Good config w/ lat-lon-radius"
    (advisorycircular/validate-config
     {:adsbx {:api-key "my-adsbx-key"
              :url "http://adsbx"}
      :aircraft-info-db-path "/"
      :pelias {:url "http://pelias/"}
      :lat 0
      :lon 1
      :radius-km 5}))
  (testing "Bad config missing radius"
    (is (thrown-with-msg?
         js/Error #"Missing configuration values.*radius"
         (advisorycircular/validate-config
          {:adsbx {:api-key "my-adsbx-key"
                   :url "http://adsbx"}
           :aircraft-info-db-path "/"
           :pelias {:url "http://pelias/"}
           :lat 0
           :lon 1}))))
  (testing "Bad config missing everything"
    (is (thrown-with-msg?
         js/Error #"Missing configuration values.*adsbx"
         (advisorycircular/validate-config {}))))
  (testing "Bad config missing lat-lon-radius"
    (is (thrown-with-msg?
         js/Error #"Missing configuration values.*lat.*lon.*radius-km.*OR.*icaos"
         (advisorycircular/validate-config
          {:adsbx {:api-key "my-adsbx-key"
                   :url "http://adsbx"}
           :aircraft-info-db-path "/"
           :pelias {:url "http://pelias/"}}))))
  (testing "Bad config with icaos instead of lat/lon/radius"
    (advisorycircular/validate-config
     {:adsbx {:api-key "my-adsbx-key"
              :url "http://adsbx"}
      :aircraft-info-db-path "/"
      :pelias {:url "http://pelias/"}
      :icaos ["123" "456"]})))


(deftest adsbx-url
  (is (= (advisorycircular/adsbx-url
          {:url "http://adsbx"
           :lat 0
           :lon 1
           :radius-nm 2})
         "http://adsbx/lat/0/lon/1/dist/2.0"))
  (is (= (advisorycircular/adsbx-url
          {:url "http://adsbx"
           :lat 0
           :lon 1
           :radius-nm 2
           :rapid-api? true})
         "http://adsbx/lat/0/lon/1/dist/2"))
  (is (thrown-with-msg?
       js/Error #"Need all of :lat, :lon, and :radius-nm"
       (advisorycircular/adsbx-url
        {:url "http://adsbx"
         :lat 0
         :rapid-api? true})))
  (is (= (advisorycircular/adsbx-url
          {:url "http://adsbx"})
         "http://adsbx/all"))
  (is (thrown-with-msg?
       js/Error #"Must specify.*lat.*RapidAPI"
       (advisorycircular/adsbx-url
        {:url "http://adsbx"
         :rapid-api? true}))))


(deftest adsbx-headers
  (is (= (advisorycircular/adsbx-headers
          {:url "http://adsbx/lat/0/lon/1/dist/2"
           :api-key "key"})
         {:api-key "key"}))
  (is (= (advisorycircular/adsbx-headers
          {:url "http://adsbx/lat/0/lon/1/dist/2"
           :api-key "key"
           :api-whitelist "whitelist"})
         {:api-key "key"
          :ADSBX-WL "whitelist"}))
  (is (= (advisorycircular/adsbx-headers
          {:url "http://adsbx/lat/0/lon/1/dist/2"
           :api-key "key"
           :rapid-api? true})
         {:x-rapidapi-key "key"
          :x-rapidapi-host "adsbx"
          :useQueryString true})))


(deftest remove-blocked-icaos
  (let [aircraft {:aircraft [{:icao "abc"} {:icao "ADEF"} {:icao "123"} {:icao "124"}]}]
    (is (= (advisorycircular/remove-blocked-icaos aircraft '())
           {:aircraft [{:icao "abc"} {:icao "ADEF"} {:icao "123"} {:icao "124"}]}))
    (is (= (advisorycircular/remove-blocked-icaos aircraft '("a.*"))
           {:aircraft [{:icao "123"} {:icao "124"}]}))
    (is (= (advisorycircular/remove-blocked-icaos aircraft '("a.*" "123"))
           {:aircraft [{:icao "124"}]}))))


(deftest keep-specified-icaos
  (let [aircraft {:aircraft [{:icao "abc"} {:icao "ADEF"} {:icao "123"} {:icao "124"}]}]
    (is (= (advisorycircular/keep-specified-icaos aircraft '())
           {:aircraft [{:icao "abc"} {:icao "ADEF"} {:icao "123"} {:icao "124"}]}))
    (is (= (advisorycircular/keep-specified-icaos aircraft '("123" "adef"))
           {:aircraft [{:icao "ADEF"} {:icao "123"}]}))))
