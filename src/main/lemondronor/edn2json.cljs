(ns lemondronor.edn2json
  (:require
   ["fs" :as fs]
   [kitchen-async.promise :as p]
   [cljs.reader :as reader]
   [fipp.edn :as fippedn]))

(def fs-promises (.-promises fs))


(defn read-file [path options]
  (.readFile fs-promises path (clj->js options)))


(defn write-file [path data options]
  (.writeFile fs-promises path data (clj->js options)))


(defn read-file-json [path]
  (p/let [json-str (read-file path {:encoding "utf-8"})
          db (js->clj (.parse js/JSON json-str))]
    db))

(defn write-file-json [db path]
  (write-file path (.stringify js/JSON (clj->js db) nil "  ") {:encoding "utf-8"}))

(defn read-file-edn [path]
  (p/let [edn-str (read-file path {:encoding "utf-8"})
          db (reader/read-string edn-str)]
    db))


(defn now []
  (.getTime (js/Date.)))


(defn main [& args]
  (p/let [db (read-file-edn (nth args 0))
          done (write-file-json db (nth args 1))]))
